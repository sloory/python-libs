from pathlib import Path
from tempfile import NamedTemporaryFile

import pytest

from file_accessor.file import FileAccessor


def test_read_not_exists_file():
    file = FileAccessor(Path('not_exists_file.txt'))
    with pytest.raises(FileNotFoundError):
        file.read()


def test_write_and_read():
    with NamedTemporaryFile() as tmp_file:
        file = FileAccessor(Path(tmp_file.name))
        file.write('test data')
        assert file.read() == 'test data'
