import pytest

from file_accessor.file import FakeAccessor


def test_read_without_content():
    with pytest.raises(FileNotFoundError):
        FakeAccessor().read()


def test_read_written_content():
    file = FakeAccessor()
    file.write('test data')
    assert file.read() == 'test data'
