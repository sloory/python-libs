from pathlib import Path
from tempfile import NamedTemporaryFile
import pytest
from cryptography.fernet import Fernet

from file_accessor.backup import BackupFileAccessor
from file_accessor.file import FileAccessor
from file_accessor.encrypt import EncryptFileAccessor


def test_read_not_exists_file():
    file = BackupFileAccessor(
        FileAccessor(Path('unexisting_file.txt')),
        FileAccessor(Path('unexisting_file_backup.txt'))
    )

    with pytest.raises(FileNotFoundError):
        file.read()


def test_write_and_read():
    with NamedTemporaryFile() as tmp_file:
        file = BackupFileAccessor(
            FileAccessor(Path(tmp_file.name)),
            FileAccessor(Path(tmp_file.name + '_backup'))
        )
        file.write('test data')
        assert file.read() == 'test data'


def test_backup_erased():
    with NamedTemporaryFile() as tmp_file:
        file = BackupFileAccessor(
            FileAccessor(Path(tmp_file.name)),
            FileAccessor(Path(tmp_file.name + '_backup'))
        )
        file.write('test data')
        assert not file.exists()


def test_works_with_encrypted_backup():
    file = NamedTemporaryFile().name
    backup_file = file + '.backup'

    key = Fernet.generate_key()
    encrypt_file = EncryptFileAccessor(
        FileAccessor(Path(file)),
        key
    )
    encrypt_backup = EncryptFileAccessor(
        FileAccessor(Path(backup_file)),
        key
    )

    encrypt_file.write('How ya doing?')

    file = BackupFileAccessor(encrypt_file, encrypt_backup)
    file.write('Test data')
    assert file.read() == 'Test data'
