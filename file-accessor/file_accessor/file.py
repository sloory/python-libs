import json
import os
from pathlib import Path
from abc import abstractmethod, ABC
from typing import AnyStr


class FileAccessorInterface(ABC):

    @abstractmethod
    def read(self) -> str: ...

    @abstractmethod
    def write(self, data: AnyStr): ...

    @abstractmethod
    def remove(self): ...

    @abstractmethod
    def exists(self) -> bool: ...


class FileAccessor(FileAccessorInterface):

    def __init__(self, path: Path):
        self.path = path

    def read(self) -> str:
        if not self.path.exists():
            raise FileNotFoundError(f"Не найден файл ({self.path})")

        with open(self.path, "r") as file:
            file_data = file.read()

        return file_data

    def write(self, data: AnyStr):
        with open(self.path, "wb" if isinstance(data, bytes) else "w") as file:
            file.write(data)

    def exists(self) -> bool:
        return self.path.exists()

    def remove(self):
        os.remove(self.path)

    def __repr__(self):
        return json.dumps(
            {
                "path": repr(self.path),
            }
        )


class FakeAccessor(FileAccessorInterface):

    def __init__(self):
        self.content = None

    def read(self) -> str:
        if self.content is None:
            raise FileNotFoundError()

        return self.content

    def write(self, data: AnyStr):
        self.content = data

    def remove(self):
        # nothing to do
        pass

    def exists(self) -> bool:
        return True

    def __repr__(self):
        return json.dumps(
            {
                "path": None
            }
        )
