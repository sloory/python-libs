import json
from typing import AnyStr
from file_accessor.file import FileAccessorInterface


class BackupFileAccessor(FileAccessorInterface):
    def __init__(
        self,
        file_accessor: FileAccessorInterface,
        backup_accessor: FileAccessorInterface
    ):
        self.file_accessor = file_accessor
        self.backup_file = backup_accessor

    def read(self) -> str:
        data = self.file_accessor.read()
        if len(data) == 0:
            self._restore()
        return self.file_accessor.read()

    def write(self, data: AnyStr):
        self.backup_file.write(data)
        self._check_data(self.backup_file, data)
        self.file_accessor.write(data)
        self._check_data(self.file_accessor, data)
        self.remove()

    def remove(self):
        self.backup_file.remove()

    def exists(self) -> bool:
        return self.backup_file.exists()

    def _backup_not_empty(self) -> bool:
        try:
            data = self.backup_file.read()
        except FileNotFoundError:
            return False

        return len(data) > 0

    def _restore(self):
        backup_data = self.backup_file.read()
        self.file_accessor.write(backup_data)
        self._check_data(self.file_accessor, backup_data)
        self.remove()

    def _check_data(self, file_accessor: FileAccessorInterface, data: AnyStr):
        if file_accessor.read() != data:
            raise Exception('Data inconsistent.')

    def __repr__(self):
        return json.dumps(
            {
                "backup_exists": self.exists(),
            }
        )
